Markdown==3.2.2
mkdocs==1.1.2
mkdocs-material==5.3.2
mkdocs-material-extensions==1.0
Pygments==2.6.1
pymdown-extensions==7.1
jinja2==3.0.3