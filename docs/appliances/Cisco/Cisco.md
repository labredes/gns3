# Instalación de appliance Cisco:

1) Descargar la appliance correspondiente al dispositivo que se quiera instalar: <a href="https://gns3.com/marketplace/appliances" target="_blank">enlace</a>
	
En este ejemplo se considera la appliance del enrutador "Cisco 3640".

2) Importar la appliance en el menú: "File > Import appliance"

![](gns3_import_apliance.png)

3) Seleccionar dónde se va a alojar el dispositivo virtual. Por defecto dentro del la misma PC local:

![](gns3_import_apliance1.png)

4) Seleccionar la versión de Cisco que se ha de instalar. Descargar de internet la imagen del dispositivo e importarla.

![](gns3_import_apliance2.png)

En el caso de este ejemplo, se debe conseguir el archivo de imagen: "c3640-a3js-mz.124-25d.image"

5) Continuar los pasos hasta finalizar la importación.

![](gns3_import_apliance4.png)
