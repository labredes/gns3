# Instalación de GNS3

Sitio web de descarga: [www.gns3.com/software/download](https://www.gns3.com/software/download)

## Instalación en Ubuntu

Seguir los siguientes comandos de instalación (si no se puede ejecutar add-apt-repository, revisar la solución en la sección "[Errores comunes](https://labredes.gitlab.io/gns3/Errores_Comunes/)"):

```bash
sudo add-apt-repository ppa:gns3/ppa
sudo apt update                                
sudo apt install gns3-gui gns3-server
sudo apt install dynamips ubridge gnome-terminal
```

Vease que **dynamips**, **ubridge** y **gnome-terminal** son programas extras a gns3, pero necesarios para su funcionamiento.

En caso de tener inconvenientes en la utilización de gns3, ver la sección "[Errores comunes](https://labredes.gitlab.io/gns3/Errores_Comunes/)", donde se listan posibles soluciones.
